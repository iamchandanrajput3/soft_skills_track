## Full-Text Search 
As our project is going through some performance and scaling issues, so I have investigated some of the full-text search engines:
- Elasticsearch
- Solr

**Elasticsearch**

It's an open source full-text search and analytics engine. It is accessible from a RESTful web service interface and uses JSON (JavaScript Object Notation) documents to store data. 

It is built on Java and as you know that Java is portable, so elasticsearch is also portable and can run on different platforms.

We can use elasticsearch to implement various things like simple search, fuzzy search, autocompletion & instant search.

Let's take an example of google search.
* You give keywords in search and get results based on more search relevant scores. 
* Sometimes we misspell words while searching but we get results for the word closely related to that keyword. 
* Also, when we are typing something in search, it shows us some suggestions based on our previous activities.

We can implement all these features using elasticsearch.

### Concept of Elasticsearch
In RDMS, we store data in form of tables. But that's not how we store in elasticsearch. We save data in the form of *JSON document*. You can think of document like a row in RDBMS.

Documents that have similar characteristics are stored in an **index**. You can think of index as a table. Then the index is divided into multiple parts called shards because of which querying and searching data is easy and fast.

To understand this suppose we have very large amount of data on a single system then it becomes very difficult to manage and query data and it slows down searching. That's why index is divided into multiple shards which can be hosted on multiple machines and due to that querying and searching becomes fast.

Also, in elasticsearch shards have replicas. The benefit is that if any of the shard is down then replicas will be used.

### Installation 
* First, you need to check Java version on your system.
  * For windows OS, use this command in command prompt -
  ```
  >java -version 
  ```
  * For Unix OS
  ```
  $echo $JAVA_HOME
  ```
* Based on your OS, download the elasticsearch and extract the file and complete the installation.
  * In Linux you can use commands :
  ```
  $wget -o [location to store file] [URL of download]

  $ tar -xzf [filename]
  ```

**Solr**

As both elasticsearch and solr are built using Apache Lucene library, so they support very similar features. But they differ in some functionalities.

* Solr has many data sources options like JDBC, XML, CSV, Microsoft word documents, PDF files. While elasticsearch supports only JSON format.
* Elasticsearch is much easier to install and configure than Solr. In Solr, you need the managed-schema file to define structure of index,fields and its types. While in elasticsearch you don't need to do that.
* Elasticsearch requires more memory for configuration than solr.
* Better caching in elasticsearch.Solr uses global caching means single cache instance for all the segments, so any change in segments needs whole cache refreshing and that's not happening in elasticsearch.
After every configuration change in Solr you need to restart the node but that's not the case in elasticsearch.
* For Node discovery, elasticsearch uses its own tool- Zen. Solr uses Apache Zookeeper with external ensemble that requires at least 3 Zookeeper instances to discover nodes.
* Elasticsearch is very dynamic while solr is static. For example- if a new node joins or leaves a cluster, elasticsearch can move shards automatically by using API call while solr needs manual moving.


>>>>Both of these are good to work with. But selection of tool may depend on the use cases and the type of requirement like solr is more suited for search applications that use massive amounts of static data while elasticsearch is more suited to modern web applications where data is carried in and out in JSON format.

### References
* [Elasticsearch Tutorial](https://www.tutorialspoint.com/elasticsearch/index.htm)
* [Solr Tutorial](https://www.tutorialspoint.com/apache_solr/index.htm)
* [Difference between Elasticsearch and Solr](https://www.hcltech.com/blogs/elasticsearch-vs-solr)
* [https://youtu.be/yZJfsUOHJjg](https://youtu.be/yZJfsUOHJjg)